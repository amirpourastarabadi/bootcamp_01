<?php


const DISCOUNT = 20;

function calcGravity(int $m1, int $m2, int $r){
    
    try{

        return 100 * ($m1 * $m2 ) / ($r * $r);
    }catch(Throwable $e){
        throw new Exception("r can not be 0");
    }

}


try{

    echo calcGravity(50, 70, 0);
}catch(Exception $e){
    echo "{$e->getMessage()}";
}